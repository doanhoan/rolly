package cz.albertov.rolly;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Switch;
import java.util.Random;
import android.support.design.widget.Snackbar;
import android.os.Vibrator;
import com.github.tbouron.shakedetector.library.ShakeDetector;

public class MainActivity extends AppCompatActivity {

    public static final Random random = new Random();
    public Button rollDice;
    public Switch modeSwitch, compSwitch;
    public ImageView img1, img2;
    public static int randomDiceValue() {
        return random.nextInt(6) + 1;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        rollDice = findViewById(R.id.btn_Roll);
        img1 = findViewById(R.id.imageView1);
        img2 = findViewById(R.id.imageView2);
        modeSwitch = findViewById(R.id.sw_Mode);
        compSwitch = findViewById(R.id.sw_Comp);

        //Rolls dice with the button press
        rollDice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                rollIt();
            }
        });

        //Rolls dice with a double shake
        ShakeDetector.create(this, new ShakeDetector.OnShakeListener() {
            @Override
            public void OnShake() {
                ShakeDetector.updateConfiguration(1.0F, 2);
                rollIt();
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        ShakeDetector.start();
    }

    @Override
    protected void onStop() {
        super.onStop();
        ShakeDetector.stop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        ShakeDetector.destroy();
    }



    public void rollIt()
    {
        //Assigns random value 1-6
        int valueOne = randomDiceValue();
        int valueTwo = randomDiceValue();

        //Gets the image resources
        int res1 = getResources().getIdentifier("d" + Integer.toString(valueOne), "drawable", "cz.albertov.rolly");
        int res2 = getResources().getIdentifier("d" + Integer.toString(valueTwo), "drawable", "cz.albertov.rolly");

        if (modeSwitch.isChecked()) //Rolling two dices
        {
            img1.setImageResource(res1);
            img2.setVisibility(View.VISIBLE);
            img2.setImageResource(res2);

            if (valueOne == valueTwo && compSwitch.isChecked()) //Notifies when two dices are the same
            {
                showWinSnBar();
                vibrate();
            }
        }
        else //Rolling one dice
        {
            img1.setImageResource(res1);
            img2.setVisibility(View.GONE); //Second dice is not in use

            if (valueOne == 6 && compSwitch.isChecked()) //Notifies if number 6 was rolled
            {
                showWinSnBar();
                vibrate();
            }
        }

    }

    public void vibrate()
    {
        Vibrator v = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
        v.vibrate(300);
    }

    public void showWinSnBar()
    {
        Snackbar snackbar=Snackbar.make(findViewById(R.id.myConstraintLayout),"YOU GOT IT!",Snackbar.LENGTH_LONG);
        snackbar.show();
    }
}
